Sending Ethereum to the blockchain network. Allows users to send transactions to the blockchain. Each transaction then paired with a gift and it will be forever stored on the blockchain. And all these transactions are visible in the allication.

Technology Stack
================
Web3.0 (Implement Web 3.0 into our react application)
Blockchain
Solidity (Write smart contracts)
ReactJS
    Vite for react initialization
Metamask (Connect to the ethereum wallet)
Hostinger - For the domain name and for hosting.
Docker - To create the development environment

Functionalities
================
1. Connect Wallet - 
    * Immediately trigger a metamask connection and it is going to ask us which account do you want to connect. 
    * Then select the account. 
    * Connected account address will update in the GUI.
    * Openning metamask will display the account has connected with the network. (And can see account balance info in the metamask.)

2. Sending ethereum to another account. 
    * Add the below details
        - Receivers address
        - Amount
        - Key word
        - Message
    * Send to the receiver
    * Metamask will ask for the confirmation and confirm from metamask
    * Metamask second notification appears for the contract interaction
    * Interact with solidity ethereum smart contract
    * Transaction will send

3. View transactions
    * Each transaction is saved in the blockchain forever
    * Can see below details 
        - From: We can see the address of the account that send that ethereum
                If click on the account it will open up in the etherscan and it can see the transactions of the address
        - To
        - Amount
        - Message
        - Gift

