Create the client folder and navigate to it.

Table of content
    1. Technologies
    2. Setup React with Vite
    3. Setup Tailwindcss
    4. Create file and folder structure in React Application
    5. Styling the application
    6. Navbar setup
        className attribute meaning
        Tags
    7. Create a component in react for NavBarItem
    8. Create customized NavBar for mobile devices


Technologies:
=============
* React
    Vite

* Styling - Tailwindcss

Setup React with Vite
======================
1. Initialize the project with Vite
    npm init vite@latest
        Give a 
            project name - folder name f the project (can say current folder - this project - ./)
            package name - give a package name (this project - share-crypt)
            select a framework (this project - react)
            select a variant (this project react)

2. Add packages

    npm install

3. Run the client side web application
    npm run dev
        
Setup Tailwindcss
==================

Follow instructions on the official website, create react app section - https://tailwindcss.com/docs/guides/create-react-app

1. Install all the necessary packages and dependencies
    npm install -D tailwindcss postcss autoprefixer

2. Initialize the project

    npx tailwindcss init -p

    This will create two config files
        * tailwind.config.js
        * postcss.config.js
         
3. Update Tailwond.configs.js with below configs

    module.exports = {
    content: ["./src/**/*.{html,js}"],
    theme: {
        extend: {},
    },
    plugins: [],
    }

4. Update src/index.css with below content (can remove existing default content)

    @tailwind base;
    @tailwind components;
    @tailwind utilities;

5. App.jsx update jsx return statement content with below content

        <h1 className="text-3xl font-bold underline">
        Hello world!
        </h1>

    * Entire App.jsx content would look like below 

        import { useState } from 'react'
        import logo from './logo.svg'
        import './App.css'

        function App() {
        const [count, setCount] = useState(0)

        return (
            <div className="App">
                <h1 className="text-3xl font-bold underline">
                    Hello world!
                </h1>
            </div>
        )
        }

        export default App

    * Refactor as below

        const App = () => {

        return (
            <div className="App">
                <h1 className="text-3xl font-bold underline">
                    Hello world!
                </h1>
            </div>
        )
        }

        export default App

    This is the initial react app. This will show "Hello world!" in an empty web page.

Create file and folder structure in React Application
======================================================

1. Create components folder
2. List all the components in src/components/
    * Navbar
    * Footer
    * Loader
    * Services
    * Transactions
    * Welcome

3. Simple components content add as below

    const Navbar = () => {
        return (
            <h1>Navbar</h1>
        );
    }

    export default Navbar;

4. jsx vs js
    * jsx means going to write jsx code, meaning to react code
    * Can use js extension too

5. Allow the application to import all these cmponents in one line, create one file named index.js inside the components folder.
Export all the components from the index.js 

export { default as Loader } from './Loader';

6. Import all the components from App.jsx

7. Update App to return components structure as below.

    <div className="min-h-screen">
      <div className="gradient-bg-welcome">
        <Navbar />
        <Welcome />
      </div>
      <Services />
      <Transactions />
      <Footer />
    </div>

This will finish the basic file and folder structure.

Styling the application
========================
1. Get a font 

        @import url("https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700&display=swap");

2. Set styles to other components

        * html {
        padding: 0;
        margin: 0;
        box-sizing: border-box;
        }

        body {
        margin: 0;
        font-family: "Open Sans", sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        }

        .gradient-bg-welcome {
        background-color:#0f0e13;
        background-image: 
            radial-gradient(at 0% 0%, hsla(253,16%,7%,1) 0, transparent 50%), 
            radial-gradient(at 50% 0%, hsla(225,39%,30%,1) 0, transparent 50%), 
            radial-gradient(at 100% 0%, hsla(339,49%,30%,1) 0, transparent 50%);
        }

        .gradient-bg-services {
        background-color:#0f0e13;
        background-image: 
            radial-gradient(at 0% 0%, hsla(253,16%,7%,1) 0, transparent 50%), 
            radial-gradient(at 50% 100%, hsla(225,39%,25%,1) 0, transparent 50%);
        }

        .gradient-bg-transactions {
        background-color: #0f0e13;
        background-image: 
            radial-gradient(at 0% 100%, hsla(253,16%,7%,1) 0, transparent 50%), 
            radial-gradient(at 50% 0%, hsla(225,39%,25%,1) 0, transparent 50%);
        }

        .gradient-bg-footer {
        background-color: #0f0e13;
        background-image: 
            radial-gradient(at 0% 100%, hsla(253,16%,7%,1) 0, transparent 53%), 
            radial-gradient(at 50% 150%, hsla(339,49%,30%,1) 0, transparent 50%);
        }

        .blue-glassmorphism {
        background: rgb(39, 51, 89, 0.4);
        border-radius: 16px;
        box-shadow: 0 4px 30px rgba(0, 0, 0, 0.2);
        backdrop-filter: blur(5px);
        -webkit-backdrop-filter: blur(5px);
        border: 1px solid rgba(0, 0, 0, 0.3);
        }

        /* white glassmorphism */
        .white-glassmorphism {
        background: rgba(255, 255, 255, 0.05);
        border-radius: 16px;
        backdrop-filter: blur(5px);
        -webkit-backdrop-filter: blur(5px);
        border: 1px solid rgba(255, 255, 255, 0.3);
        }

        .eth-card {
        background-color:#a099ff;
        background-image: 
            radial-gradient(at 83% 67%, rgb(152, 231, 156) 0, transparent 58%), 
            radial-gradient(at 67% 20%, hsla(357,94%,71%,1) 0, transparent 59%), 
            radial-gradient(at 88% 35%, hsla(222,81%,65%,1) 0, transparent 50%), 
            radial-gradient(at 31% 91%, hsla(9,61%,61%,1) 0, transparent 52%), 
            radial-gradient(at 27% 71%, hsla(336,91%,65%,1) 0, transparent 49%), 
            radial-gradient(at 74% 89%, hsla(30,98%,65%,1) 0, transparent 51%), 
            radial-gradient(at 53% 75%, hsla(174,94%,68%,1) 0, transparent 45%);
        }

        .text-gradient {
        background-color: #fff;
        background-image: radial-gradient(at 4% 36%, hsla(0,0%,100%,1) 0, transparent 53%), radial-gradient(at 100% 60%, rgb(0, 0, 0) 0, transparent 50%);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        }

3. Navbar and Welcome components have separate gradient with above styles

Navbar setup
============

1. Import few icons

import { HiMenuAlt4 } from "react-icons/hi";
import { AiOutlineClose } from "react-icons/ai";

2. Import logo image

import logo from '../../images/logo.png'

3. Install dependencies/packages for import icons and to use ethers in the backend

npm install react-icons ethers

4. Navbar explanation

<nav className="w-full flex md:justify-center justify-between items-center p-4">

</nav>



className attribute meaning
----------------------------

<nav className="w-full flex md:justify-center justify-between items-center p-4">

In class-based components, the className attribute is used to set or return the value of an element's class attribute. Using this property, the user can change the class of an element to the desired class.

Above className value string has several classes. Meaning of each className can findout from the official tailwind documentation. (https://tailwindcss.com/)

* w-full
    w stands for width
    w-[value] can give a percentage
    w-full means div going to take the entire percentage of the web

    https://tailwindcss.com/docs/width

* Layout->Display->
    flex
        Utilities for controlling how flex items both grow and shrink. 
        Utilities for controlling the display box type of an element.

        https://tailwindcss.com/docs/flex

        https://tailwindcss.com/docs/display


* md:justify-center justify-between
    On medium devices justify going to be centered
    On other deviceds justify going to be in between

    If defines with md: then rest will be the class for medium devices and later part for othercase

* justify-center
    Utilities for controlling how flex and grid items are positioned along a container's main axis.
    https://tailwindcss.com/docs/justify-content

* items-center
    Utilities for controlling how flex and grid items are positioned along a container's cross axis.
    https://tailwindcss.com/docs/align-items

* p-4
    Utilities for controlling an element's padding.

    https://tailwindcss.com/docs/padding


<div className="md:flex-[0.5] flex-initial justify-center items-center">

* md:flex-[0.5] flex-initial
    medium devices flex and otherwise flex-initial


<img src={logo} alt="logo" className="w-32 cursor-pointer" />

* cursor-pointer

    Utilities for controlling the cursor style when hovering over an element.

    https://tailwindcss.com/docs/cursor

<ul className="text-white md:flex hidden list-none flex-row justify-between items-center flex-initial">

* text-white

    Utilities for controlling the text color of an element.

    https://tailwindcss.com/docs/text-color

* Layout->Display->

    hidden
        Utilities for controlling the display box type of an element.

    https://tailwindcss.com/docs/display

* list-none
    Utilities for controlling the bullet/number style of a list.

    https://tailwindcss.com/docs/list-style-type#setting-the-list-style-type

* Flexbox and Grid -> Flex Direction 
    Utilities for controlling the direction of flex items.

    flex-row

        https://tailwindcss.com/docs/flex-direction#row

* Flexbox and Grid -> Flex
    Utilities for controlling how flex items both grow and shrink.

    flex-initial
        Use flex-initial to allow a flex item to shrink but not grow, taking into account its initial size:

* mx-4

    Margin on horizontal axis

* custom classProps can pass to function as a variable and render within the function using argument name
    ex: classProps 

    const NavBarItem = ({ title, classprops }) => (
        <li className={`mx-4 cursor-pointer ${classprops}`}>{title}</li>
    );


<li className="bg-[#2952e3] py-2 px-7 mx-4 rounded-full cursor-pointer hover:bg-[#2546bd]">Login</li>

* bg-[#2952e3]
    Background colour
        https://tailwindcss.com/docs/background-color

* py-2
    y axis top and bottom padding

* px-7
    left and right padding

mx-4
    margin in x azis left and right

* rounded-full
    corners of the widget
        https://tailwindcss.com/docs/border-radius#rounded-corners

* hover:bg-[#2546bd]
hover
    dyncamically change when hover the widget
    bg-[#2546bd] will update the color incase hover




Tags
====

1. 2. nav vs div 

Technically they work the same way, but for search engines that tells them there is a navigation area.

<nav> 
    meant for more semantic markup (letting the browser know) that those links aren't just normal links, they are a navigation menu. This can be useful for accessibility (tabs, mobile devices,browsers, such as screen readers for disabled users, can use this element to determine whether to omit the initial rendering of this content.) and other things.

    For More reference on <nav> : https://www.w3.org/TR/html51/sections.html#the-nav-element

<div> 
    tag defines a division or a section in an HTML document. It is a block-level element that is commonly used to identify large groupings of content, and which helps to build a web page’s layout and design using CSS.

    For more reference on <div> : http://www.html-5-tutorial.com/div-tag.htm

When it comes to styling there is no big difference.

3. <img>


4. alt tag

An alt tag, also known as "alt attribute" and "alt description," is an HTML attribute applied to image tags to provide a text alternative for search engines. Applying images to alt tags such as product photos can positively impact an ecommerce store's search engine rankings.

5. <ul>

6. <li>



unordered list


Create a component in react for NavBarItem
===========================================
* This is a functional component
* It will accept few properties
    title
    classProps
* It will return a list item
    <li> tag item
* It can pass className values as anargument 
* It can pass list item text as an argument

    const NavBarItem = ({ title, classprops }) => (
        <li className={`mx-4 cursor-pointer ${classprops}`}>{title}</li>
    );

* Then can call this as an Component inside the Nav or Div tag

    <ul className="text-white md:flex hidden list-none flex-row justify-between items-center flex-initial">
        <NavBarItem/>
    </ul>

Dynamic Block
--------------
* Can create multiple NavbarItems from a dynamic block
{ } - Dynamic Block
[ ] - Array
For an Array we can call map method
    map takes a parameter
        1. Function

And it will apply function to the current value in the array

For this example it passes the below Arrow function

    (item, index) => (
                        <NavBarItem key={item + index} title={item} />
    ))

Final NavBarItem usage
-----------------------
<ul className="text-white md:flex hidden list-none flex-row justify-between items-center flex-initial">
    {["Market", "Exchange", "Tutorials", "Wallets"].map((item, index) => (
        <NavBarItem key={item + index} title={item} />
    ))}
</ul>


